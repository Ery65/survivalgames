package pl.mc4e.ery65.survivalgames.arena;

import java.util.Map;

import org.bukkit.Location;
import org.bukkit.inventory.InventoryHolder;

import com.google.common.collect.Maps;

public class ChestManager {
    
    private Map<Location, InventoryHolder> chests = Maps.newHashMap();
    private Map<Location, Integer> ChestLvL = Maps.newHashMap();
    
    public ChestManager(){}
    
    public void addChest(Location loc, InventoryHolder holder){
        Location a = normalize(loc);
        chests.put(a, holder);
        ChestLvL.put(a, 0);
    }
    
    public void addChest(Location loc, InventoryHolder holder, int lvl){
        Location a = normalize(loc);
        chests.put(a, holder);
        ChestLvL.put(a, lvl);
    }
    
    private Location normalize(Location loc){
        if (loc == null)
            throw new NullPointerException("Location can\'t be null!");
        return new Location(loc.getWorld(), loc.getBlockX(), loc.getBlockY(), loc.getBlockZ());
    }
    
    public boolean contains(Location loc){
        return chests.containsKey(normalize(loc));
    }
    
    public boolean isDefaultChest(Location loc){
        return ChestLvL.get(normalize(loc)) == 0;
    }
    
    public int getChestLvL(Location loc){
        return ChestLvL.get(normalize(loc));
    }
    
    public InventoryHolder getChest(Location loc){
        return chests.get(normalize(loc));
    }
    
    public void remove(Location loc){
        chests.remove(normalize(loc));
        check();
    }
    
    private void check(){
        if (chests == null || chests.isEmpty()){
            chests = Maps.newHashMap();
        }
    }
    
}
