package pl.mc4e.ery65.survivalgames.arena;

import org.bukkit.Location;
import org.bukkit.util.Vector;

import pl.mc4e.ery65.survivalgames.api.SimpleArena;

public class CircleArena extends SimpleArena {
    
    private int radius;
    
    protected CircleArena(){}

    public CircleArena(Location loc1, int radius) {
        super(loc1);
        this.radius = radius;
    }
    
    public Location getCenterLocation(){
        return getFirst();
    }
    
    @Override
    public boolean contains(Location loc){
        return contains(loc);
    }
    
    @Override
    public String toString(){
        return getClass().getSimpleName() + "(" + getFirst().toString() + "\\|"+ radius + ")";
    }
    
    public boolean isIn(Location loc){
        return getFirst().distance(loc) <= radius;
    }
    
    public boolean isIn(Vector vector){
        return getFirst().toVector().distance(vector) <= radius;
    }

}
