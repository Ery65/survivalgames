package pl.mc4e.ery65.survivalgames.arena;

import org.bukkit.Location;

import pl.mc4e.ery65.survivalgames.api.SimpleArena;

public class OtherArena extends SimpleArena {
    
    protected OtherArena(){}
    
    public OtherArena(Location center, Location... walls){
        super(center, walls);
    }
    
    public Location getFirst(){
        return locations.get(0);
    }
    
    public Location get(int index){
        if (index < locations.size()){
            return locations.get(index);
        } else
            return null;
    }
    
    public Location[] getAllLocations(){
        return (Location[]) locations.toArray();
    }
    
    public boolean contains(Location loc){
        return locations.contains(loc);
    }
    
    public boolean contains(int index){
        return locations.size() > index;
    }
    
    public Location getLast(){
        return locations.get(locations.size()-1);
    }

}
