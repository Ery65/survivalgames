package pl.mc4e.ery65.survivalgames.managers;

import java.util.List;
import java.util.Map;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.inventory.ItemStack;

import pl.mc4e.ery65.survivalgames.api.SimpleKit;
import pl.mc4e.ery65.survivalgames.configuration.KitsConfig;
import pl.mc4e.ery65.survivalgames.utils.ItemsUtil;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

public class KitsManager {
    
    private ItemStack[] it = new ItemStack[0]; 
    
    private Map<KitInfo, List<SimpleKit>> kits = Maps.newHashMap(); 
    
    public KitsManager(){
        loadKits(KitsConfig.getSection("Kits"));
    }
    
    public KitsManager(ConfigurationSection cfg){
        loadKits(cfg);
    }
    
    private void loadKits(ConfigurationSection cfs){
        if (cfs != null){
            for (String key : cfs.getKeys(false)){
                int slot = cfs.getInt(key + ".slot", -1);
                KitInfo info = new KitInfo(key, slot);
                List<SimpleKit> kits = getList(info);
                ConfigurationSection cfg = cfs.getConfigurationSection("items");
                if (cfg != null){
                    for (String keys : cfg.getKeys(false)){
                        String permission = cfg.getString(keys + ".permission");
                        double price = cfg.getDouble(keys + ".price", -1);
                        int lvl = parseLvL(key);
                        List<ItemStack> items = Lists.newArrayList();
                        for (String item : cfg.getStringList(keys + ".items")){
                            items.add(ItemsUtil.ParseItemFromString(item));
                        }
                        if (items.size() == 1){
                            kits.add(new SimpleKit(key, permission, lvl, items.get(0), price, (ItemStack[]) null));
                        } else {
                            kits.add(new SimpleKit(key, permission, lvl, items.get(0), price,
                                    items.toArray(it)));
                        }
                        this.kits.put(info, kits);
                    }
                } else
                    continue;
            }
        }
    }
    
    private int parseLvL(String lvl){
        int i;
        try {
            i = Integer.parseInt(lvl);
        } catch (NumberFormatException ignored){
            return -3;
        }
        return i;
    }
    
    private List<SimpleKit> getList(KitInfo info){
        List<SimpleKit> kits;
        if (this.kits.containsKey(info)){
            kits = this.kits.get(info);
        } else
            kits = Lists.newArrayList();
        return kits;
    }
    
    private class KitInfo {
        
        @SuppressWarnings("unused")
        public String type;
        
        @SuppressWarnings("unused")
        public int slot;
        
        public KitInfo(String type, int slot){
            this.type = type;
            this.slot = slot;
        } 
    }
    
}
