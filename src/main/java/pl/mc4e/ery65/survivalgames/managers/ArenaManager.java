package pl.mc4e.ery65.survivalgames.managers;

import pl.mc4e.ery65.survivalgames.api.SimpleArena;
import pl.mc4e.ery65.survivalgames.arena.ChestManager;
import pl.mc4e.ery65.survivalgames.arena.CircleArena;
import pl.mc4e.ery65.survivalgames.arena.OtherArena;
import pl.mc4e.ery65.survivalgames.arena.SpawnManager;
import pl.mc4e.ery65.survivalgames.configuration.ArenaConfig;

public class ArenaManager {
    
    private static SpawnManager spawnManager;
    
    private static SimpleArena arenaManager;
    
    private static ChestManager chestManager;
    
    public ArenaManager(){
        loadDefault();
        
        if (ArenaConfig.type.equalsIgnoreCase("circle")){
            arenaManager = new CircleArena(ArenaConfig.center, ArenaConfig.radius);
        } else{
            arenaManager = new OtherArena(ArenaConfig.center);
        }
    }
    
    public ArenaManager(SimpleArena manager){
        loadDefault();
        
        arenaManager = manager;
    }
    
    private void loadDefault(){
        spawnManager = new SpawnManager();
        
        chestManager = new ChestManager();
    }
    
    public void chengeDefaultArena(SimpleArena arena){
        arenaManager = arena;
    }
    
    public static SimpleArena getArenaManager(){
        return arenaManager;
    }
    
    public static ChestManager getChestManager(){
        return chestManager;
    }
    
    public static SpawnManager getSpawnManager(){
        return spawnManager;
    }

}
