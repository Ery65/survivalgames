package pl.mc4e.ery65.survivalgames.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import pl.mc4e.ery65.survivalgames.SurvivalGames;
import pl.mc4e.ery65.survivalgames.configuration.PluginConfig;

public class PlayerListener implements Listener {
    
    @EventHandler
    void onJoin(PlayerJoinEvent e){
        if (PluginConfig.ENABLE_MYSQL){
            SurvivalGames.getMySQL().refresh();
            SurvivalGames.getMySQL().contains(e.getPlayer().getName());
        }
    }
    
    @EventHandler
    void onDisconnet(PlayerQuitEvent e){
        if (SurvivalGames.getPlayerManger().contains(e.getPlayer())){
            SurvivalGames.getPlayerManger().removePlayer(e.getPlayer());
        }
    }

}
