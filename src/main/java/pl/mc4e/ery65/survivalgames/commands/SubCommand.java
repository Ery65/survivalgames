package pl.mc4e.ery65.survivalgames.commands;

import org.bukkit.command.CommandSender;

public interface SubCommand {
    
    public void runCommand(CommandSender cs, String cmd, String[] args);

}
