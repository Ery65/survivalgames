package pl.mc4e.ery65.survivalgames.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import pl.mc4e.ery65.survivalgames.configuration.ArenaConfig;

public class SpawnCommand implements CommandExecutor {
    
    public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args){
        
        if (args.length == 2){
            if (args[0].equalsIgnoreCase("set")){
                ArenaConfig.save("Spawns." + 1, null);
            }
        }
        
        return true;
    }

}
