package pl.mc4e.ery65.survivalgames;

public enum GameState {

    WAITING(0),
    WAIT(1),
    SPEEDY_WAIT(2),
    STARTING(3),
    START(4),
    KITS(11),
    BLITZ(22),
    GAME(5),
    PREPARE_TO_DEATCHMATH(6),
    DEATHMATCH(7),
    END(8);
    
    private int number;
    
    GameState(int value){
        number = value;
    }
    
    public int getIntValue(){
        return number;
    }
    
    public static GameState getNextState(GameState current){
        if (current != END){
            return getFromInt((current.getIntValue()+1));
        } else {
            return WAITING;
        }
    }
    
    public static GameState getFromInt(int value){
        for (GameState state : values()){
            if (state.getIntValue() == value)
                return state;
        }
        throw new IllegalStateException("Values must be beetwen 0 and 8!");
    }
}
