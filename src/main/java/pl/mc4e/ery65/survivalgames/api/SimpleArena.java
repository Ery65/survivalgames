package pl.mc4e.ery65.survivalgames.api;

import java.util.List;

import org.bukkit.Location;

import com.google.common.collect.Lists;

public abstract class SimpleArena {
    
    protected List<Location> locations = Lists.newArrayList();
    
    protected SimpleArena(){}
    
    protected SimpleArena(Location loc1){
        locations.add(loc1);
    }
    
    protected SimpleArena(Location center, Location... loc){
        locations.set(0, center);
        for (Location l : loc){
            locations.add(l);
        }
    }
    
    protected Location getFirst(){
        return locations.get(0);
    }
    
    protected boolean contains(Location loc){
        return locations.contains(loc);
    }
    
    protected boolean contains(int index){
        return locations.size() > index;
    }
    
    protected Location getLast(){
        return locations.get(locations.size()-1);
    }
    
}
