package pl.mc4e.ery65.survivalgames.api;

import java.util.Collection;

import org.bukkit.inventory.ItemStack;

public class SimpleKit {
    
    protected String type;
    protected String permission;
    
    protected int level = -3;
    
    protected ItemStack item;
    
    protected ItemStack[] items;
    
    protected double price = -1;
    
    public SimpleKit(String type, String perm, int lvl, ItemStack item,
            double price, ItemStack... items){
        this(type, perm, price, item, items);
        level = lvl;
    }
    
    public SimpleKit(String type, String permission, double price, ItemStack item, ItemStack... items){
        this(type, price, item, items);
        this.permission = permission;
    }
    
    public SimpleKit(String type, double price, ItemStack item, ItemStack... items){
        this(price, item, items);
        this.type = type;
    }
    
    public SimpleKit(double price, ItemStack item, ItemStack... items){
        this.price = price;
        this.item = item;
        this.items = items;
    }
    
    public SimpleKit(double price, String perm, ItemStack item, ItemStack... items){
        this(price, item, items);
        permission = perm;
    }
    
    public boolean hasType(){
        return type != null;
    }
    
    public boolean hasLvL(){
        return level != -3;
    }
    
    public boolean hasItems(){
        return items != null && items.length != 1;
    }
    
    public boolean hasPermission(){
        return permission != null && !permission.isEmpty();
    }
    
    public boolean isFree(){
        return price <= 0;
    }
    
    public void setLvL(int value){
        level = value;
    }
    
    public void setPrice(double value){
        price = value;
    }
    
    public void setPermission(String perm){
        permission = perm;
    }
    
    public void setType(String currentType){
        type = currentType;
    }
    
    public void setItem(ItemStack it){
        item = it;
    }
    
    public void setItems(ItemStack... it){
        items = it;
    }
    
    public void setItems(Collection<ItemStack> items){
        ItemStack[] its = new ItemStack[0];
        this.items = items.toArray(its);
    }
    
    public String getType(){
        return type;
    }
    
    public String getPermission(){
        return permission;
    }
    
    public int getLvL(){
        return level;
    }
    
    public double getPrice(){
        return price;
    }
    
    public ItemStack getItem(){
        return item;
    }
    
    public ItemStack[] getItems(){
        return items;
    }

}
