package pl.mc4e.ery65.survivalgames.configuration;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import pl.mc4e.ery65.survivalgames.SurvivalGames;

public class ArenaConfig {
    
    private static FileConfiguration cfg;
    
    private static File file;
    
    public static boolean FINISHED;
    
    public static int radius;
    
    public static Location center;
    
    public static String type;
    
    public static List<Location> spawnLocations;
    
    static {
        file = new File(SurvivalGames.getInstance().getDataFolder() + File.separator + "arena.yml");
        cfg = YamlConfiguration.loadConfiguration(file);
        
        FINISHED = validate();
    }
    
    public static ConfigurationSection getSection(String path){
        return cfg.getConfigurationSection(path);
    }
    
    private static boolean validate(){
        return false;
    }
    
    public static void save(String patch, Object o){
        cfg.set(patch, o);
        save();
    }
    
    private static void save(){
        try {
            cfg.save(file);
        } catch (IOException ignored){
            //ignored
        }
    }

}
