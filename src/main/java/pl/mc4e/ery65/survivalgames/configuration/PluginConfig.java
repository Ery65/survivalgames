package pl.mc4e.ery65.survivalgames.configuration;

import java.io.File;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import pl.mc4e.ery65.survivalgames.SurvivalGames;

public class PluginConfig {
    
    private static FileConfiguration cfg;
    
    public static boolean ENABLE_MYSQL;
    public static boolean USE_BUNGEE;
    public static boolean ENABLE_BLITZ;
    
    public static String USERNAME;
    public static String PASSWORD;
    public static String HOST;
    public static String DATABASE;
    public static String LOBBY;
    
    public static int TOKENS_PER_KILL;
    public static int TOKENS_PER_KILL_VIP;
    public static int TOKENS_PER_KILL_SVIP;
    public static int TOKENS_REMOVE_PER_DEATH;
    public static int TOKENS_REMOVE_PER_DEATH_VIP;
    public static int TOKENS_REMOVE_PER_DEATH_SVIP;
    public static int TOKENS_PER_GAME_WIN;
    public static int TOKENS_PER_GAME_WIN_VIP;
    public static int TOKENS_PER_GAME_WIN_SVIP;
    public static int TOKENS_REMOVE_PER_LOST_GAME;
    public static int TOKENS_REMOVE_PER_LOST_GAME_VIP;
    public static int TOKENS_REMOVE_PER_LOST_GAME_SVIP;
    public static int PORT;
    
    public static long WAIT_TIME;
    public static long WAIT_TIME_MIN_PLAYERS;
    public static long WAIT_TIME_MAX_PLAYERS;
    public static long CENTER_WAIT;
    public static long GAME_TIME;
    public static long KIT_WAIT;
    public static long BLITZ_WAIT;
    public static long WAIT_DEATHMATCH;
    public static long DEATHMATCH_MAX_TIME;
    
    static {
        
        File file = new File(SurvivalGames.getInstance().getDataFolder() + File.separator + "config.yml");
        if (!file.exists()){
            SurvivalGames.getInstance().getDataFolder().mkdirs();
            SurvivalGames.copy(SurvivalGames.getInstance().getResource("Resources/config.default"), file);
        }
        cfg = YamlConfiguration.loadConfiguration(file);
        
        ConfigurationSection mysql = cfg.getConfigurationSection("MySQL");
        
        if (mysql != null){
            ENABLE_MYSQL = mysql.getBoolean("enable", false);
            
            PORT = mysql.getInt("port", 3306);
            
            USERNAME = mysql.getString("user", "root");
            PASSWORD = mysql.getString("password", "root");
            HOST = mysql.getString("host", "localhost");
            DATABASE = mysql.getString("database", "minecraft");
        }
        
        ConfigurationSection tokens = cfg.getConfigurationSection("Tokens");
        
        if (tokens != null){
        	TOKENS_PER_KILL = tokens.getInt("per_kill", 0);
            TOKENS_PER_KILL_VIP = tokens.getInt("per_kill_vip", 0);
            TOKENS_PER_KILL_SVIP = tokens.getInt("per_kill_svip", 0);
            TOKENS_REMOVE_PER_DEATH = tokens.getInt("remove_per_death", 0);
            TOKENS_REMOVE_PER_DEATH_VIP = tokens.getInt("remove_per_death_vip", 0);
            TOKENS_REMOVE_PER_DEATH_SVIP = tokens.getInt("remove_per_death_svip", 0);
            TOKENS_PER_GAME_WIN = tokens.getInt("per_game_win", 0);
            TOKENS_PER_GAME_WIN_VIP = tokens.getInt("per_game_win_vip", 0);
            TOKENS_PER_GAME_WIN_SVIP = tokens.getInt("per_game_win_svip", 0);
            TOKENS_REMOVE_PER_LOST_GAME = tokens.getInt("remove_per_lost_game", 0);
            TOKENS_REMOVE_PER_LOST_GAME_VIP = tokens.getInt("remove_per_lost_game_vip", 0);
            TOKENS_REMOVE_PER_LOST_GAME_SVIP = tokens.getInt("remove_per_lost_game_svip", 0);
        }
        
        ConfigurationSection timers = cfg.getConfigurationSection("Timers");
        
        if (timers != null){
            WAIT_TIME = timers.getLong("wait", 90);
            WAIT_TIME_MIN_PLAYERS = timers.getLong("wait_min_players", 30);
            WAIT_TIME_MAX_PLAYERS = timers.getLong("wait_max_players", 5);
            CENTER_WAIT = timers.getLong("wait_center", 30);
            GAME_TIME = timers.getLong("game_time", 900);
            KIT_WAIT = timers.getLong("wait_kits", 40);
            BLITZ_WAIT = timers.getLong("wait_blitz", 300);
            WAIT_DEATHMATCH = timers.getLong("wait_deathmatch", 15);
            DEATHMATCH_MAX_TIME = timers.getLong("deathmatch_max_time", -1);
        }
    }

}
