package pl.mc4e.ery65.survivalgames;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import pl.mc4e.ery65.survivalgames.commands.BasicCommand;
import pl.mc4e.ery65.survivalgames.configuration.PluginConfig;
import pl.mc4e.ery65.survivalgames.data.MySQL;
import pl.mc4e.ery65.survivalgames.listeners.PlayerListener;
import pl.mc4e.ery65.survivalgames.managers.ArenaManager;
import pl.mc4e.ery65.survivalgames.managers.PlayerManager;
import pl.mc4e.ery65.survivalgames.utils.Connector;

public class SurvivalGames extends JavaPlugin {
    
    private static SurvivalGames instance;
    
    private static MySQL sql;
    
    private static ArenaManager arenaManager;
    
    private GameState current;
    
    private static BasicCommand bcmd;
    
    private Threads gameThreads;
    
    private static PlayerManager playerManager;
    
    private int task = 0;
    
    public void onEnable(){
        instance = this;
        
        if (PluginConfig.ENABLE_MYSQL){
            sql = new MySQL();
        }
        
        arenaManager = new ArenaManager();
        
        getServer().getPluginManager().registerEvents(new PlayerListener(),  this);
        
        gameThreads = new Threads();
        
        bcmd = new BasicCommand();
        
        playerManager = new PlayerManager();
        
        task = Bukkit.getScheduler().scheduleSyncRepeatingTask(instance, new Runnable(){
            
            public void run(){
                gameThreads.onTick();
            }
            
        }, 20L, 20L);
        
        Map<String, Map<String, Object>> commands =  getDescription().getCommands();
        for (Map.Entry<String, Map<String, Object>> command : commands.entrySet()){
            //registerCommand();
            System.out.println(command.getKey());
            for (Map.Entry<String, Object> values : command.getValue().entrySet()){
                System.out.println(values.getKey());
                System.out.println(values.getValue());
            }
        }
    }
    
    public void onDisable(){
        playerManager.saveAllShutdown();
        Connector.connectLobbyAll();
        Bukkit.getScheduler().cancelTask(task);
    }
    
    public void setState(GameState current){
        this.current = current;
    }
    
    public GameState currentState(){
        return current;
    }
    
    public static ArenaManager getArenaManger(){
        return arenaManager;
    }
    
    public static PlayerManager getPlayerManger(){
        return playerManager;
    }
    
    public static MySQL getMySQL(){
        return sql;
    }
    
    public static BasicCommand getBasicComamnd(){
        return bcmd;
    }
    
    public static void copy(InputStream resource, File target){
        OutputStream out;
        try {
            out = new FileOutputStream(target);
            byte[] buffer = new byte[1024];
            int size = 0;
            
            while ((size = resource.read(buffer)) != -1) {
                out.write(buffer, 0, size);
            }

            out.close();
            resource.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public static SurvivalGames getInstance(){
        return instance;
    }
    
}
