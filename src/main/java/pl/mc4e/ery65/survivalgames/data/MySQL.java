package pl.mc4e.ery65.survivalgames.data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import pl.mc4e.ery65.survivalgames.configuration.PluginConfig;

public class MySQL {
    
    private Connection conn;
    
    private String name;
    
    private long lastRefresh = 0;
    
    public MySQL(){
        OpenConnection();
        if (PluginConfig.ENABLE_BLITZ)
        	name = "blitz_survival_games";
        else
        	name = "survival_games";
        if (conn != null)
            createTable();
    }
    
    public String getName(){
    	return name;
    }
    
    public void OpenConnection(){
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection("jdbc:mysql://" + PluginConfig.HOST + ":" +
            PluginConfig.PORT + "/" + PluginConfig.DATABASE + "?autoReconnect=true",
            PluginConfig.USERNAME, PluginConfig.PASSWORD);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    public synchronized void refresh(){
        if (lastRefresh > System.currentTimeMillis())
            return;
        try {
            if (conn != null && !conn.isClosed()){
                try {
                    conn.isValid(1);
                } catch (SQLException e) {
                    try {
                        conn.close();
                    } catch (SQLException e1) {
                        // ignored
                        System.out.println("one exception");
                        e1.printStackTrace();
                    }
                    OpenConnection();
                }
            } else
                OpenConnection();
        } catch (SQLException e){
            OpenConnection();
        }
        lastRefresh = System.currentTimeMillis() + 300000;
    }
    
    public Connection getConnection(){
        return conn;
    }
    
    public boolean contains(String name){
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            st = conn.prepareStatement("SELECT * FROM `" + name + "` WHERE `name`=? LIMIT 1");
            st.setString(1, name);
            rs = st.executeQuery();
            if (rs.next())
                return true;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeResources(st, rs);
        }
        return false;
    }
    
    public void createTable(){
        StringBuilder query = new StringBuilder();
        query.append("CREATE TABLE IF NOT EXISTS `" + name + "` ")
        .append("(`id` INT NOT NULL AUTO_INCREMENT, ")
        .append("`name` VARCHAR(16) NOT NULL UNIQUE, ")
        .append("`games_win` INT, ")
        .append("`games_lose` INT, ")
        .append("`tokens` INT DEFAULT 0, ")
        .append("`kills` INT DEFAULT 0, ")
        .append("`deaths` INT DEFAULT 0, ")
        .append("PRIMARY KEY (`name`), ")
        .append("KEY (`id`))");
        PreparedStatement st = null;
        try {
            st = conn.prepareStatement(query.toString());
            st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeResources(st, null);
        }
    }
    
    public void closeResources(PreparedStatement st, ResultSet rs){
        if (st != null){
            try {
                st.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if (rs != null){
            try {
                rs.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

}
