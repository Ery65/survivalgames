package pl.mc4e.ery65.survivalgames;

import pl.mc4e.ery65.survivalgames.configuration.PluginConfig;

public class Threads {
    
    private static long elapsedTime;
    
    public Threads(){}
    
    public void setTimeToEnd(long value){
        if (value < 0){
            throw new IllegalStateException("Time value can\'t be less that 0!");
        }
        elapsedTime = value;
        superTick();
    }
    
    public void onTick(){
        if (elapsedTime == -10)
            return;
        elapsedTime--;
        if (elapsedTime <= 0){
            changeState(SurvivalGames.getInstance().currentState());
        }
    }
    
    private void superTick(){
        if (elapsedTime == -10)
            return;
        if (elapsedTime <= 0){
            changeState(SurvivalGames.getInstance().currentState());
        }
    }
    
    public void changeState(GameState current){
        GameState newState = getNextState(current);
        SurvivalGames.getInstance().setState(newState);
        elapsedTime = getTime(newState);
    }
    
    private long getTime(GameState current){
        switch (current){
        case WAIT:
            return PluginConfig.WAIT_TIME;
        case SPEEDY_WAIT:
            return PluginConfig.WAIT_TIME_MAX_PLAYERS;
        case STARTING:
            return PluginConfig.CENTER_WAIT;
        case START:
            return PluginConfig.GAME_TIME;
        case GAME:
            return elapsedTime;
        case PREPARE_TO_DEATCHMATH:
            return PluginConfig.WAIT_DEATHMATCH;
        case DEATHMATCH:
            return PluginConfig.DEATHMATCH_MAX_TIME;
        case KITS:
            return elapsedTime;
        case BLITZ:
            return elapsedTime;
            default:
                return -10;
        }
    }
    
    public GameState getNextState(GameState current){
        return GameState.getNextState(current);
    }
    
}
